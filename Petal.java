/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A2
File: Petal.java
Note: Some skeleton code taken from 2D Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.geom.*;
import java.util.ArrayList;
import java.util.Random;
import javax.vecmath.*;

public class Petal {
	ArrayList<MyShape> lines;
	Point2d ptClick;
	
	int mystrokeThickness;
	
	boolean isSelected;
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	Petal(int mystrokeThickness){
		this.mystrokeThickness = mystrokeThickness;
		
		lines = new ArrayList<MyShape>();
		lines.add(new MyShape(mystrokeThickness));
		ptClick = new Point2d(-1, -1);
	}
	
	//draw
	public void paint(Graphics2D g2){
		for (int i=0; i<lines.size(); ++i){
			lines.get(i).paint(g2);
		}
	}
	
	public void deleteInstances(){
		MyShape myLine = lines.get(0);
		lines.clear();
		lines.add(myLine);
	}
	
	public MyShape addInstance(){
		MyShape newLine = new MyShape(lines.get(lines.size()-1));
		lines.add(newLine);
		return newLine;
	}
}

# Bumblebee (Patternizer)
# Created by: Lucas Connors
# for CS 349 (User Interfaces)

![Bumblebee](http://revolutiontech.ca/media/img/bumblebee.jpg)

***

## About

Bumblebee is a small program for creating simple patterns (can be good for drawing flowers or similar shapes). The project was created for an assignment in CS 349 of Spring 2013.

## How to Use

Click the circle in the center and drag your mouse outside of the circle to draw a polyline. Release the mouse to create the polyline. Then you can select the line and drag it around the circle to create identical copies and to change the size. Click on the center circle to change colour and double-click to clear the screen.

## Prerequisites

Bumblebee requires JDK, which you can install on Debian with:

`sudo apt-get install default-jdk`

## Running

Then Bumblebee can be run with the following command:

`make run`
/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A2
File: Circle.java
Note: Some skeleton code taken from 2D Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.geom.*;
import java.util.ArrayList;
import java.util.Random;
import javax.vecmath.*;

public class Circle extends Ellipse2D.Double {
	
	Random rand;
	Color randcolor;
	int mystrokeThickness;
	int colorTypes;
    ArrayList<Petal> petals;
    Petal selectedPetal;
    Boolean selected;
    double lastx;
    double lasty;
	
	Circle(double x, double y, double width, double height){
		this.x = x;															// note that the circle's coordinates are at the top-left
		this.y = y;															// of the circle. To get the center, must take:
		this.width = width;													// x+width/2, y+height/2
		this.height = height;
		lastx = this.x;
		lasty = this.y;
		
		mystrokeThickness = 3;
		
		// create random number generator
    	rand = new Random();
    	colorTypes = 0;
    	newRandColor();
    	
    	// create array list of petals
		petals = new ArrayList<Petal>();
		selectedPetal = null;
		
		selected = false;
	}
	
	// draw
    public void paint(Graphics2D g2, int centerx, int centery) {
    	// move circle to center
    	x = centerx-width/2;
    	y = centery-height/2;
    	
    	// if the screen has been resized, move all the petals
    	if (x != lastx || y != lasty){
    		for (int i=0; i<petals.size(); ++i){
    			if (petals.get(i) != null){
    				Petal thisPetal = petals.get(i);
    				for (int j=0; j<thisPetal.lines.size(); ++j){
    					if (thisPetal.lines.get(j) != null && thisPetal.lines.get(j).points != null){
							MyShape thisLine = thisPetal.lines.get(j);
							for (int k=0; k<thisLine.points.size(); ++k) {
								thisLine.points.get(k).x += (x-lastx);
								thisLine.points.get(k).y += (y-lasty);
								thisLine.hasChanged = true;
							}
						}
					}
				}
			}
    		lastx = x;
    		lasty = y;
    	}
    	
		// draw petals
        for (int i=0; i<petals.size(); ++i){
        	if (petals.get(i) != null){
	            petals.get(i).paint(g2);
			}
		}    
    
		// draw circle
		g2.setColor(randcolor);
		g2.fill(this);
    }
	
	public void newRandColor(){
		float hue = rand.nextFloat();
		float saturation = rand.nextFloat();
		float luminance = rand.nextFloat();
		
		switch (colorTypes){
			case 0:	// all colours
				break;
			case 1: // cool colours
				hue = (rand.nextInt(210) + 80) / 360f;
				saturation = 1.0f;
				luminance = 1.0f;
				break;
			case 2: // warm colours
				hue = ((350 + rand.nextInt(70)) % 360) / 360f;		// between 0-60 or 350-360
				saturation = 1.0f;
				luminance = 1.0f;
				break;
		}
		
		randcolor = Color.getHSBColor(hue, saturation, luminance);
	}
	
	public void click(){
		selected = true;
	    petals.add(new Petal(mystrokeThickness));
        int i = petals.size()-1;
        petals.get(i).lines.get(0).setIsClosed(false);
        petals.get(i).lines.get(0).setIsFilled(false);
        petals.get(i).lines.get(0).setColour(randcolor);
	}
	
	public void doubleClick(){
		petals.removeAll(petals);
	}
	
	public void release(){
		newRandColor();
		selected = false;
	}
	
	public void drag(int x, int y){
		int i = petals.size()-1;
		petals.get(i).lines.get(0).addPoint(x, y);
    }
    
    // drag petal
	public void dragPetal(double mousex, double mousey){
    	Point2d center = new Point2d(x+width/2, y+height/2);									// create points
    	Point2d start = selectedPetal.ptClick;
    	Point2d mouse = new Point2d(mousex, mousey);
    	Point2d startOrg = new Point2d(start.x-center.x, start.y-center.y);
    	Point2d mouseOrg = new Point2d(mouse.x-center.x, mouse.y-center.y);
    	
    	Vector2d begin = new Vector2d();														// create vectors
    	begin.sub(start, center);
    	Vector2d current = new Vector2d();
    	current.sub(mouse, center);
    	
		double toBegin = Math.atan2(startOrg.y, startOrg.x);
		double toMouse = Math.atan2(mouseOrg.y, mouseOrg.x);
		double theta = toMouse - toBegin;														// find the rotation angle
		
		selectedPetal.deleteInstances();														// delete all polyline instances
		/* create polylines instances */
    	if (Math.abs((180/Math.PI)*theta) > 5){													// don't create instances with -5<=theta<=5
    		int numInstances = (int)Math.ceil(Math.PI / Math.abs(theta));
    		for (int i=0; i<numInstances; ++i){
    			MyShape thisLine = selectedPetal.addInstance();
    			thisLine.rotation = 2*Math.PI/numInstances;
    			thisLine.rotateWhole(thisLine.rotation, center);
    		}
    	}
    	
    	/* rotate the polylines */
		if (theta < 0){
			theta += 2*Math.PI;
		}
		for (int i=0; i<selectedPetal.lines.size(); ++i){
			MyShape thisLine = selectedPetal.lines.get(i);
			if (thisLine.rotation > 0){
				thisLine.rotateWhole(2*Math.PI-thisLine.rotation, center);						// rotate back to start
			}
			thisLine.rotateWhole(theta, center);												// rotate to new location
			thisLine.rotation = theta;
		}
    	
    	/* scale the polylines */
		double scaleRatio = current.length()/begin.length();									// find magnitudes and get the ratio
		if (scaleRatio != 0){																	// don't set the scale to 0
																									// (because then all information is lost)
			for (int i=0; i<selectedPetal.lines.size(); ++i){									// apply transformation matrix to all
				MyShape thisLine = selectedPetal.lines.get(i);										// of the polylines
				thisLine.scaleWhole(1/thisLine.scale, center);									// but first put the polyline back to org
				thisLine.scaleWhole(scaleRatio, center);
				thisLine.scale = scaleRatio;
			}
		}
	}
}

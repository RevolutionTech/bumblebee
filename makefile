# Written by Daniel Vogel
# Edits made by Lucas Connors
# for CS 349 A2

# super simple makefile
# call it using 'make NAME=name_of_code_file_without_extension'
# (assumes a .java extension)
NAME = "Pattern"

# HACK: myShape.java and vecmath are compiled regardless if needed
all:
	@echo "Compiling..."
	javac -cp vecmath-1.5.1.jar *.java

# HACK: adding vecmath to classpath regardless if needed
# -ea activates assert (disabled for submission)
run: all
	@echo "Running..."
	java -cp "vecmath-1.5.1.jar:." $(NAME)

/* - - - - - - - - - - - - - - - - - - - - - -
Name: Lucas Connors
Course: CS 349
Assignment: A2
File: Pattern.java
Note: Some skeleton code taken from Java Examples from the course web site
- - - - - - - - - - - - - - - - - - - - - - */

/*
* CS 349 Java Code Examples
*
* ShapeDemo    Demo of MyShape class: draw shapes using mouse.
*
*/
//import javax.swing.JFrame;
//import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.geom.*;
import javax.vecmath.*;
import javax.swing.event.MouseInputListener;
import java.awt.event.MouseEvent;

// create the window and run the demo
public class Pattern extends JPanel implements MouseInputListener {
	
	Circle circle;
    int win_width;
    int win_height;
    int radius;

    Pattern() {   
    	// set window width and height
    	win_width = 400;
    	win_height = 400;
    	// create circle
    	radius = 25;
		circle = new Circle(0, 0, radius*2, radius*2);
		//circle = new Circle(win_width/2-radius, win_height/2-radius, radius*2, radius*2);
		
        // create the window
        JFrame frame = new JFrame("Pattern"); // jframe is the app window
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(win_width, win_height); // window size
        frame.setContentPane(this); // add canvas to jframe
        frame.setVisible(true); // show the window
		
        // add listeners
        addMouseListener(this);
        addMouseMotionListener(this);
        
        // create a label
        final JLabel label = new JLabel("Stroke: 3");
        // create a slider
		JSlider slider = new JSlider(1, 10, 3);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JSlider s = (JSlider)e.getSource();
				label.setText("Stroke: " + s.getValue());
				circle.mystrokeThickness = s.getValue();
			}
		});
        
		// create a radio button group
		ButtonGroup radiobuttons = new ButtonGroup();
		JPanel radioPanel = new JPanel(new GridLayout(1, 0));
		for (String s: new String[] {"All Colours", "Cool Colours", "Warm Colours"}){
			JRadioButton rb = new JRadioButton(s);
			rb.addActionListener(radioButtonListener);
			radiobuttons.add(rb);
			radioPanel.add(rb);
			if (s == "All Colours"){
				rb.setSelected(true);
			}
		}
        
		// add widgets
		frame.setLayout(new FlowLayout());
		frame.add(label);
		frame.add(slider);
		frame.add(radioPanel);
		frame.setVisible(true);
    }

    public static void main(String[] args) {
        Pattern canvas = new Pattern();
    }
    
    // custom graphics drawing 
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g; // cast to get 2D drawing methods
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,  // antialiasing look nicer
                            RenderingHints.VALUE_ANTIALIAS_ON);
		
		// draw circle and polylines
		circle.paint(g2, getSize().width/2, getSize().height/2);
    }
    
    // calculates the difference between two points
    public double distance_between_pts(double x1, double y1, double x2, double y2){
    	return Math.sqrt(Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2));
    }

    @Override
    public void mouseClicked(MouseEvent arg0) {
    }

    @Override
    public void mouseEntered(MouseEvent arg0) {
    }

    @Override
    public void mouseExited(MouseEvent arg0) {
    }

    @Override
    public void mousePressed(MouseEvent arg0) {
		circle.selectedPetal = null;											/* deselect all polylines */
		for (int i=0; i<circle.petals.size(); ++i){
			Petal thisPetal = circle.petals.get(i);
			thisPetal.setSelected(false);
			for (int j=0; j<thisPetal.lines.size(); ++j){
				MyShape thisLine = thisPetal.lines.get(j);
				thisLine.setSelected(false);
			}
		}
		
    	// check if clicked on circle
	    if (distance_between_pts(circle.x+radius, circle.y+radius, arg0.getX(), arg0.getY()) <= radius){
			if (arg0.getClickCount() == 2){
				circle.doubleClick();
			}
			else {
				circle.click();
			}
		}
		
		// check if clicked on polylines
		else {
			for (int i=circle.petals.size()-1; i>=0; --i){						/* 	select the first line you find out	*/
				Petal thisPetal = circle.petals.get(i);							/*	of the most recently created		*/
				for (int j=0; j<thisPetal.lines.size(); ++j){
					MyShape thisLine = thisPetal.lines.get(j);
					if (thisLine.hittest(arg0.getX(), arg0.getY())){
						thisPetal.setSelected(true);
						thisPetal.lines.get(0).setSelected(true);
						circle.selectedPetal = thisPetal;
						thisPetal.ptClick.x = arg0.getX();
						thisPetal.ptClick.y = arg0.getY();
						break;
					}
				}
				if (thisPetal.isSelected()){
					break;
				}
			}
		}
		
		repaint();
    }

    @Override
    public void mouseReleased(MouseEvent arg0) {
    	if (circle.selected){
	    	circle.release();
	    }
    	repaint();
    }

    @Override
    public void mouseDragged(MouseEvent arg0) {
    	if (circle.selected){
	    	circle.drag(arg0.getX(), arg0.getY());
	    }
	    else if (circle.selectedPetal != null){
	    	circle.dragPetal(arg0.getX(), arg0.getY());
	    }
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent arg0) {
    }
    
	// create a radio button listener
	RadioButtonListener radioButtonListener = new RadioButtonListener();
	class RadioButtonListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JRadioButton rb = (JRadioButton)e.getSource();
			if (rb.getText().equals("All Colours")){
				circle.colorTypes = 0;
			}
			else if (rb.getText().equals("Cool Colours")){
				circle.colorTypes = 1;
			}
			else {
				circle.colorTypes = 2;
			}
			circle.newRandColor();
			repaint();
		}
	}
}

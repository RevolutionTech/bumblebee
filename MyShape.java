/*
*  MyShape: See ShapeDemo2 for an example how to use this class.
*
*/
import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;

import javax.vecmath.*;


// simple shape class
class MyShape {

    // shape model
    ArrayList<Point2d> points;
    Boolean isFilled = false; // shape is polyline or polygon
    Boolean isClosed = false; // polygon is filled or not
    Color colour = Color.BLACK;
	float mystrokeThickness;
	
    public Color getColour() {
		return colour;
	}
	public void setColour(Color colour) {
		this.colour = colour;
	}
    public float getStrokeThickness() {
		return mystrokeThickness;
	}
	public void setStrokeThickness(float mystrokeThickness) {
		this.mystrokeThickness = mystrokeThickness;
	}
	public Boolean getIsFilled() {
		return isFilled;
	}
	public void setIsFilled(Boolean isFilled) {
		this.isFilled = isFilled;
	}
	public Boolean getIsClosed() {
		return isClosed;
	}
	public void setIsClosed(Boolean isClosed) {
		this.isClosed = isClosed;
	}
	
	// for selection
	boolean isSelected;
	public boolean isSelected() {
		return isSelected;
	}
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	// for drawing
    Boolean hasChanged = false; // dirty bit if shape geometry changed
    int[] x_points, y_points;

	public double rotation = 0;
	public double scale = 1.0;
	
	MyShape(int mystrokeThickness){
		this.mystrokeThickness = mystrokeThickness;
	}
	MyShape(MyShape shape){
		this.points = new ArrayList<Point2d>();
		for (int i=0; i<shape.points.size(); ++i){
			this.points.add(new Point2d(shape.points.get(i).x, shape.points.get(i).y));
		}
		this.isFilled = shape.isFilled;
		this.isClosed = shape.isClosed;
		this.colour = shape.colour;
		this.mystrokeThickness = shape.mystrokeThickness;
		this.hasChanged = true;
		this.rotation = shape.rotation;
		this.scale = shape.scale;
	}
	
    // replace all points with array
    public void setPoints(double[][] pts) {
        points = new ArrayList<Point2d>();
        for (double[] p : pts) {
            points.add(new Point2d(p[0],p[1]));
        }
        hasChanged = true;
    }
    
    // add a point to end of shape
    public void addPoint(double x, double y) {
    	if (points == null)
    		points = new ArrayList<Point2d>();
    	points.add(new Point2d(x,y));
    	hasChanged = true;
    }

    
    // paint the shape
    public void paint(Graphics2D g2) {

        //update the shape in java Path2D object if it changed
        if (hasChanged) {
            x_points = new int[points.size()];
            y_points = new int[points.size()];
            for (int i=0; i < points.size(); i++) {
                x_points[i] = (int)points.get(i).x;
                y_points[i] = (int)points.get(i).y;
            }
            hasChanged = false;
        }

        //don't draw if path2D is empty (not shape)
        if (x_points != null) {
        	
        	// special draw for selection
        	if (isSelected) {
        		g2.setColor(Color.YELLOW);
        		g2.setStroke(new BasicStroke(mystrokeThickness * 4));
            	if (isClosed)
                    g2.drawPolygon(x_points, y_points, points.size());
                else
                    g2.drawPolyline(x_points, y_points, points.size());
        	}
        	
        	g2.setColor(colour);

            // call right drawing function
            if (isFilled) {
                g2.fillPolygon(x_points, y_points, points.size());
            }
            else {
            	g2.setStroke(new BasicStroke(mystrokeThickness)); 
            	if (isClosed)
                    g2.drawPolygon(x_points, y_points, points.size());
                else
                    g2.drawPolyline(x_points, y_points, points.size());
            }
            
            // set stroke back to normal
            g2.setStroke(new BasicStroke(3));
        }
    }
    
    // find closest point from ClosestPointDemo
    static Point2d closestPoint(Point2d M, Point2d P0, Point2d P1)
    {
    	Vector2d v = new Vector2d();
    	v.sub(P1,P0); // v = P2 - P1
    	
    	// early out if line is less than 1 pixel long
    	if (v.lengthSquared() < 0.5)
    		return P0;
    	
    	Vector2d u = new Vector2d();
    	u.sub(M,P0); // u = M - P1

    	// scalar of vector projection ...
    	double s = u.dot(v)  // u dot v 
    			 / v.dot(v); // v dot v
    	
    	// find point for constrained line segment
    	if (s < 0) 
    		return P0;
    	else if (s > 1)
    		return P1;
    	else {
    		Point2d I = P0;
        	Vector2d w = new Vector2d();
        	w.scale(s, v); // w = s * v
    		I.add(w); // I = P1 + w
    		return I;
    	}
    }
    
    // return perpendicular vector
    static public Vector2d perp(Vector2d a)
    {
    	return new Vector2d(-a.y, a.x);
    }
    
    // line-line intersection
    // return (NaN,NaN) if not intersection, otherwise returns intersecting point
    static Point2d lineLineIntersection(Point2d P0, Point2d P1, Point2d Q0, Point2d Q1)
    {
    	
    	// TODO: implement

    	return new Point2d();
    }
    
    // scale the whole shape
    public void scaleWhole(double scaleRatio, Point2d pt){
       	AffineTransform tOrigin = new AffineTransform(1, 0, 0, 1, -pt.x, -pt.y);
		AffineTransform T = new AffineTransform(scaleRatio, 0, 0, scaleRatio, 0, 0);    	// create transformation matrix
    	AffineTransform tBack = new AffineTransform(1, 0, 0, 1, pt.x, pt.y);
    	transformAllPts(tOrigin);
    	transformAllPts(T);
    	transformAllPts(tBack);
    	hasChanged = true;
    }
    
    // rotate the whole shape
    public void rotateWhole(double theta, Point2d pt){
    	AffineTransform tOrigin = new AffineTransform(1, 0, 0, 1, -pt.x, -pt.y);
    																						// create transformation matrix
    	AffineTransform T = new AffineTransform(Math.cos(theta), Math.sin(theta), -1*Math.sin(theta), Math.cos(theta), 0, 0);
    	AffineTransform tBack = new AffineTransform(1, 0, 0, 1, pt.x, pt.y);
    	transformAllPts(tOrigin);
    	transformAllPts(T);
    	transformAllPts(tBack);
    	hasChanged = true;
    }
    
    // transform all points in the shape
    public void transformAllPts(AffineTransform T){
    	for (int i=0; i<points.size(); ++i){
    		Point2d thisPt = points.get(i);
    		points.set(i, transform(T, thisPt));								// transform according to the transformation matrix
    	}
    }

    // affine transform helper
    // return P_prime = T * P
    Point2d transform( AffineTransform T, Point2d P) {
    	Point2D.Double p = new Point2D.Double(P.x, P.y);
    	Point2D.Double q = new Point2D.Double();
    	T.transform(p, q);
    	return new Point2d(q.x, q.y);
    }
    
    // hit test with this shape
    public boolean hittest(double x, double y){
		if (points == null || points.size() == 0){				/* if no points, return false */
			return false;
		}
    	else if (points.size() == 1){							/* if one point, check that */
    		Point2d thisPt = points.get(0);
	    	if (Math.abs(x-thisPt.x) <= 4+getStrokeThickness() && Math.abs(y-thisPt.y) <= 4+getStrokeThickness()){
	    		return true;
	    	}
	    	else {
	    		return false;
	    	}
    	}
    	else {													/* otherwise get the closest point */
	        Point2d M = new Point2d(x, y);
		    for (int i=0; i<points.size()-1; i++) {
		    	Point2d P0 = new Point2d(points.get(i).x, points.get(i).y);		// get closest point
		    	Point2d P1 = new Point2d(points.get(i+1).x, points.get(i+1).y);
		    	Point2d thisPt = closestPoint(M, P0, P1);
		    																	// check if closest point is within 2 pixels from polyline
		    	if (Math.abs(x-thisPt.x) <= 4+getStrokeThickness() && Math.abs(y-thisPt.y) <= 4+getStrokeThickness()){
		    		return true;
		    	}
		    }
	    	return false;
	    }
    }
}
